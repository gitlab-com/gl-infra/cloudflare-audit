package ReportGenerator

import (
	"encoding/json"
	"fmt"
	"gitlab.com/gitlab-com/gl-infra/cloudflare-audit/types"
	"os"
	"path"
	"regexp"
)

func GenerateAccountReports(audits []types.AccountAudit, outdir string) (err error) {
	for _, audit := range audits {
		fmt.Printf(
			"--------------------------------\n"+
				"Writing reports for account '%s'\n",
			audit.Name,
		)
		err = writeReport(outdir, audit.Name, "metadata.json", audit)
		if err != nil {
			return
		}
		err = writeReport(outdir, audit.Name, "members.json", audit.Members)
		if err != nil {
			return
		}
		err = writeReport(outdir, audit.Name, "roles.json", audit.Roles)
		if err != nil {
			return
		}
		err = writeReport(outdir, audit.Name, "custom_pages.json", audit.CustomPages)
		if err != nil {
			return
		}
	}
	return
}

func GenerateZoneReports(audits []types.ZoneAudit, outdir string) (err error) {
	for _, audit := range audits {
		fmt.Printf(
			"--------------------------------\n"+
				"Writing reports for zone '%s'\n",
			audit.Name,
		)
		fmt.Printf(audit.Summarize())

		err = writeReport(outdir, audit.Name, "metadata.json", audit)
		if err != nil {
			return
		}
		err = writeReport(outdir, audit.Name, "records.json", audit.Records)
		if err != nil {
			return
		}
		err = writeReport(outdir, audit.Name, "spectrum.json", audit.SpectrumApps)
		if err != nil {
			return
		}
		err = writeReport(outdir, audit.Name, "certs.json", audit.Certs)
		if err != nil {
			return
		}
		err = writeReport(outdir, audit.Name, "page_rules.json", audit.PageRules)
		if err != nil {
			return
		}
		err = writeReport(outdir, audit.Name, "deprecated_fw_access_rules.json", audit.DeprecatedFWAccessRules)
		if err != nil {
			return
		}
		err = writeReport(outdir, audit.Name, "waf_packages.json", audit.WAF)
		if err != nil {
			return
		}
		err = writeReport(outdir, audit.Name, "waf_overrides.json", audit.WAFOverrides)
		if err != nil {
			return
		}
		err = writeReport(outdir, audit.Name, "user_agent_rules.json", audit.UserAgentRules)
		if err != nil {
			return
		}
		err = writeReport(outdir, audit.Name, "temp_user_agent_rules.json", audit.TemporaryUserAgentRules)
		if err != nil {
			return
		}
		err = writeReport(outdir, audit.Name, "settings.json", audit.Settings)
		if err != nil {
			return
		}
		err = writeReport(outdir, audit.Name, "log_push.json", audit.LogpushJobs)
		if err != nil {
			return
		}
		err = writeReport(outdir, audit.Name, "custom_pages.json", audit.CustomPages)
		if err != nil {
			return
		}
		err = writeReport(outdir, audit.Name, "worker_routes.json", audit.WorkerRoutes)
		if err != nil {
			return
		}
		err = writeReport(outdir, audit.Name, "rate_limits.json", audit.RateLimits)
		if err != nil {
			return
		}
		err = writeReport(outdir, audit.Name, "temp_rate_limits.json", audit.TemporaryRateLimits)
		if err != nil {
			return
		}
		err = writeReport(outdir, audit.Name, "lockdown_rules.json", audit.LockdownRules)
		if err != nil {
			return
		}
		err = writeReport(outdir, audit.Name, "firewall_rules.json", audit.FirewallRules)
		if err != nil {
			return
		}
		err = writeReport(outdir, audit.Name, "temp_firewall_rules.json", audit.TemporaryFirewallRules)
		if err != nil {
			return
		}

		for _, pkg := range audit.WAF {
			err = writeReport(outdir, path.Join(audit.Name, "waf_packages"), fmt.Sprintf("%s.json", pkg.Name), pkg)
			if err != nil {
				return
			}
			for _, group := range pkg.Groups {
				err = writeReport(outdir, path.Join(audit.Name, "waf_packages", sanitizeName(pkg.Name)), fmt.Sprintf("%s.json", group.Name), group)
				if err != nil {
					return
				}
			}
		}
	}

	return
}

var sanitizer = regexp.MustCompile(`(?m)[^a-zA-Z0-9\.\/]+`)

func sanitizeName(in string) string {
	return sanitizer.ReplaceAllString(in, "_")
}

func writeReport(outdir string, subdir string, fileName string, data interface{}) (err error) {
	fileName = sanitizeName(fileName)
	err = os.MkdirAll(path.Join(outdir, subdir), 0750)
	if err != nil {
		err = fmt.Errorf("MkdirAll error %s", err.Error())
		return
	}
	var file *os.File
	file, err = os.OpenFile(path.Join(outdir, subdir, fileName), os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0600)
	if err != nil {
		err = fmt.Errorf("OpenFile error %s", err.Error())
		return
	}
	defer file.Close()

	encoder := json.NewEncoder(file)
	encoder.SetIndent("", "  ")
	err = encoder.Encode(data)
	if err != nil {
		err = fmt.Errorf("Encode error %s", err.Error())
	}
	return
}
