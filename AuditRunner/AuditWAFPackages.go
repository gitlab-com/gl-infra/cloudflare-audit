package AuditRunner

import (
	"github.com/cloudflare/cloudflare-go"
	"github.com/sirupsen/logrus"
	"gitlab.com/gitlab-com/gl-infra/cloudflare-audit/types"
)

func (runner *AuditRunner) auditWAFByZone(log *logrus.Entry, audit *types.ZoneAudit, zoneID string) (err error) {
	var packages []cloudflare.WAFPackage
	{
		log.Printf("loading WAF packages...")
		packages, err = runner.api.ListWAFPackages(runner.Context, zoneID)
		if err != nil {
			return
		}
		log.Printf("loading WAF packages %d", len(packages))
	}

	for _, pkg := range packages {
		audit.WAF[pkg.ID] = types.WAFAuditPackage{
			ID:            pkg.ID,
			Name:          pkg.Name,
			Description:   pkg.Description,
			DetectionMode: pkg.DetectionMode,
			Sensitivity:   pkg.Sensitivity,
			ActionMode:    pkg.ActionMode,
			Groups:        map[string]types.WAFAuditGroup{},
		}
		log = log.WithField("package", pkg.Name)

		var groups []cloudflare.WAFGroup
		{
			log.Printf("loading WAF groups...")
			groups, err = runner.api.ListWAFGroups(runner.Context, zoneID, pkg.ID)
			if err != nil {
				return
			}
			for _, group := range groups {
				audit.WAF[pkg.ID].Groups[group.ID] = types.WAFAuditGroup{
					ID:            group.ID,
					Name:          group.Name,
					Description:   group.Description,
					ModifiedRules: group.ModifiedRulesCount,
					Mode:          group.Mode,
					Rules:         map[string]types.WAFAuditRule{},
				}
			}
			log.Printf("loading WAF groups %d", len(groups))
		}

		var rules []cloudflare.WAFRule
		{
			log.Printf("loading WAF rules...")
			rules, err = runner.api.ListWAFRules(runner.Context, zoneID, pkg.ID)
			if err != nil {
				return
			}
			for _, rule := range rules {
				audit.WAF[pkg.ID].Groups[rule.Group.ID].Rules[rule.ID] = types.WAFAuditRule{
					ID:          rule.ID,
					Description: rule.Description,
					Priority:    rule.Priority,
					Mode:        rule.Mode,
					DefaultMode: rule.DefaultMode,
				}
			}
			log.Printf("loading WAF rules %d", len(rules))
		}
	}

	return
}
