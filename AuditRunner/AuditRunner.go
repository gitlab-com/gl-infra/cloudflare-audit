package AuditRunner

import (
	"context"
	"errors"
	"github.com/cloudflare/cloudflare-go"
)

type AuditRunner struct {
	api      *cloudflare.API
	Zones    []cloudflare.Zone
	Accounts []string
	Context  context.Context
}

var ErrNoZones = errors.New("no listable zones")

func NewAuditRunner(cloudflare_token string) (runner *AuditRunner, err error) {
	var CloudflareAPI *cloudflare.API
	CloudflareAPI, err = cloudflare.NewWithAPIToken(cloudflare_token)
	if err != nil {
		return
	}

	runner = &AuditRunner{
		api:     CloudflareAPI,
		Context: context.Background(),
	}

	{
		runner.Zones, err = CloudflareAPI.ListZones(runner.Context)
		if err != nil {
			return
		}

		if len(runner.Zones) <= 0 {
			err = ErrNoZones
			return
		}
	}

	{
		var info cloudflare.ResultInfo
		var tmp []cloudflare.Account
		var page = 1
		var maxPage = 1
		for page <= maxPage {
			tmp, info, err = CloudflareAPI.Accounts(runner.Context, cloudflare.PaginationOptions{
				Page:    page,
				PerPage: 50,
			})
			if err != nil {
				return
			}
			page++
			maxPage = info.TotalPages
			for _, acc := range tmp {
				runner.Accounts = append(runner.Accounts, acc.ID)
			}
		}
	}

	return
}
