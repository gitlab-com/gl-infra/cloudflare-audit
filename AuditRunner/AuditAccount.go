package AuditRunner

import (
	"github.com/cloudflare/cloudflare-go"
	"github.com/sirupsen/logrus"
	"gitlab.com/gitlab-com/gl-infra/cloudflare-audit/types"
)

func (runner *AuditRunner) AuditAccount(accId string) (audit types.AccountAudit, err error) {
	var log *logrus.Entry
	log = logrus.WithField("account_id", accId)
	var info cloudflare.ResultInfo
	{
		log.Printf("loading accounts details...")
		var acc cloudflare.Account
		acc, info, err = runner.api.Account(runner.Context, accId)
		if err != nil {
			return
		}
		audit.ID = acc.ID
		audit.Name = acc.Name
		audit.Settings = acc.Settings
		audit.Members = map[string]types.MemberAudit{}
		audit.Roles = map[string]cloudflare.AccountRole{}
		audit.CustomPages = map[string]cloudflare.CustomPage{}
		log.Println("loading accounts details done")
	}
	log = log.WithField("account", audit.Name)

	{
		var tmp []cloudflare.AccountMember
		log.Printf("loading members...")
		var page = 1
		var maxPage = 1
		for page <= maxPage {
			{
				tmp, info, err = runner.api.AccountMembers(runner.Context, audit.ID, cloudflare.PaginationOptions{
					Page:    page,
					PerPage: 50,
				})
				if err != nil {
					return
				}
				page++
				maxPage = info.TotalPages
			}
			for _, member := range tmp {
				for _, role := range member.Roles {
					audit.Roles[role.Name] = role
				}

				var roles []string
				for _, role := range member.Roles {
					roles = append(roles, role.Name)
				}
				audit.Members[member.User.Email] = types.MemberAudit{
					ID:     member.ID,
					Code:   member.Code,
					User:   member.User,
					Status: member.Status,
					Roles:  roles,
				}
			}
		}
		log.Printf("loading members %d", len(audit.Members))
	}

	{
		log.Printf("loading Custom pages...")
		var tmp []cloudflare.CustomPage
		tmp, err = runner.api.CustomPages(runner.Context, &cloudflare.CustomPageOptions{
			AccountID: audit.ID,
		})
		if err != nil {
			return
		}
		for _, page := range tmp {
			if page.State != "default" {
				audit.CustomPages[page.ID] = page
			}
		}
		log.Printf("loading Custom pages %d", len(audit.CustomPages))
	}

	if audit.Name == "" {
		audit.Name = audit.ID
	}

	return
}
