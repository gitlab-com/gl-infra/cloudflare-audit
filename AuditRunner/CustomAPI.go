package AuditRunner

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/cloudflare/cloudflare-go"
	"gitlab.com/gitlab-com/gl-infra/cloudflare-audit/types"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

var ErrTryAgain = errors.New("try again")

/**
 * callAPI: Is a rudamentary API caller using the supplied auth.
 */
func (runner *AuditRunner) callAPI(method, uri string, params interface{}) (respBody []byte, err error) {
	// Replace nil with a JSON object if needed
	var jsonBody []byte

	if params != nil {
		if paramBytes, ok := params.([]byte); ok {
			jsonBody = paramBytes
		} else {
			jsonBody, err = json.Marshal(params)
			if err != nil {
				return nil, fmt.Errorf("error marshalling params to JSON: %s", err.Error())
			}
		}
	} else {
		jsonBody = nil
	}

	var resp *http.Response
	var reqBody io.Reader
	if jsonBody != nil {
		reqBody = bytes.NewReader(jsonBody)
	}

	uri = fmt.Sprintf("%s/%s", runner.api.BaseURL, strings.TrimLeft(uri, "/"))

	req, err := http.NewRequest(method, uri, reqBody)
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", runner.api.APIToken))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("User-Agent", runner.api.UserAgent)
	req = req.WithContext(runner.Context)
	if err != nil {
		err = fmt.Errorf("could not build request %s", err.Error())
		return
	}

	c := &http.Client{
		Timeout: 2 * time.Minute,
	}

	resp, err = c.Do(req)
	if err != nil {
		err = fmt.Errorf("could not perform request %s", err.Error())
		return
	}
	if resp.StatusCode == http.StatusTooManyRequests || resp.StatusCode >= 500 {
		err = ErrTryAgain
		return
	}

	respBody, err = ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if err != nil {
		err = fmt.Errorf("could not read response body %s", err.Error())
		return
	}

	switch {
	case resp.StatusCode >= http.StatusOK && resp.StatusCode < http.StatusMultipleChoices:
	default:
		var s string
		if respBody != nil {
			s = string(respBody)
		}
		return nil, fmt.Errorf("HTTP status %d: content %q", resp.StatusCode, s)
	}

	return respBody, nil
}

func (runner *AuditRunner) listCertificatePacks(zone_id string) (packs []types.CFCertificatePack, err error) {
	uri := "zones/" + zone_id + "/ssl/certificate_packs"
	res, err := runner.callAPI("GET", uri, nil)
	if err != nil {
		return nil, fmt.Errorf("error from callAPI %s", err.Error())
	}
	type result struct {
		Result []types.CFCertificatePack `json:"result"`
	}
	var r result
	if err := json.Unmarshal(res, &r); err != nil {
		return nil, fmt.Errorf("error from json.Unmarshal. Content: %s, error: %s", string(res), err.Error())
	}
	return r.Result, nil
}

func (runner *AuditRunner) listCustomCertificates(zone_id string) (packs []types.CFCert, err error) {
	uri := "zones/" + zone_id + "/custom_certificates"
	res, err := runner.callAPI("GET", uri, nil)
	if err != nil {
		return nil, fmt.Errorf("error from callAPI %s", err.Error())
	}
	type result struct {
		Result []types.CFCert `json:"result"`
	}
	var r result
	if err := json.Unmarshal(res, &r); err != nil {
		return nil, fmt.Errorf("error from json.Unmarshal. Content: %s, error: %s", string(res), err.Error())
	}
	return r.Result, nil
}

func (runner *AuditRunner) getDNSSEC(zone_id string) (dnssec *types.CFDNSSEC, err error) {
	uri := "zones/" + zone_id + "/dnssec"
	res, err := runner.callAPI("GET", uri, nil)
	if err != nil {
		return nil, fmt.Errorf("error from callAPI %s", err.Error())
	}
	type result struct {
		Result types.CFDNSSEC `json:"result,omitempty"`
	}
	var r result
	if err := json.Unmarshal(res, &r); err != nil {
		return nil, fmt.Errorf("error from json.Unmarshal. Content: %s, error: %s", string(res), err.Error())
	}
	return &r.Result, nil
}

// Reimplementation of cloudflare.FirewallRules. The upstream version does not paginate.
func (runner *AuditRunner) getAllFirewallRules(zoneID string) (rules []cloudflare.FirewallRule, err error) {
	uri := fmt.Sprintf("zones/%s/firewall/rules", zoneID)

	var page = 1
	var maxPage = 1
	for page <= maxPage {
		var reqUrl = fmt.Sprintf("%s?page=%d", uri, page)
		var res []byte

		res, err = runner.callAPI("GET", reqUrl, nil)
		if err != nil {
			return
		}

		var firewallDetailResponse cloudflare.FirewallRulesDetailResponse
		err = json.Unmarshal(res, &firewallDetailResponse)
		if err != nil {
			return
		}

		rules = append(rules, firewallDetailResponse.Result...)
		maxPage = firewallDetailResponse.TotalPages
		page++
	}

	return
}
