package AuditRunner

import (
	"encoding/json"
	"fmt"
	"github.com/cloudflare/cloudflare-go"
	"github.com/sirupsen/logrus"
	"gitlab.com/gitlab-com/gl-infra/cloudflare-audit/types"
	"reflect"
	"strings"
)

func (runner *AuditRunner) AuditZone(zone cloudflare.Zone) (audit types.ZoneAudit, err error) {
	var log *logrus.Entry
	log = logrus.WithField("zone_id", zone.ID).WithField("zone", zone.Name)

	audit.Initialize()

	{
		// Reload details, because I am not sure how complete the input is
		log.Printf("loading zone details...")
		zone, err = runner.api.ZoneDetails(runner.Context, zone.ID)
		if err != nil {
			return
		}
		audit.Name = zone.Name
		audit.ID = zone.ID
		if zone.DevMode < 0 {
			audit.DevMode = 0
		} else {
			audit.DevMode = zone.DevMode
		}
		audit.ModTime = zone.ModifiedOn.Unix()
		audit.NameServers = zone.NameServers
		audit.Status = zone.Status
		audit.Paused = zone.Paused
		audit.Type = zone.Type
		audit.VanityNS = zone.VanityNS
		audit.DeactReason = zone.DeactReason
		log.Println("loading zone details done")
	}

	{
		log.Printf("loading DNSSEC...")
		var tmp *types.CFDNSSEC
		tmp, err = runner.getDNSSEC(zone.ID)
		if err != nil {
			return
		}
		audit.DNSSEC = tmp
		log.Printf("loading DNSSEC ok: %t", tmp != nil)
	}

	{
		log.Printf("loading Argo Smart Routing...")
		var tmp cloudflare.ArgoFeatureSetting
		tmp, err = runner.api.ArgoSmartRouting(runner.Context, zone.ID)
		if err != nil {
			return
		}
		audit.ArgoSmartRouting = tmp.Value == "on"
		log.Printf("loading Argo Smart Routing: %t", audit.ArgoSmartRouting)
	}

	{
		log.Printf("loading Universal SSL setting...")
		var setting cloudflare.UniversalSSLSetting
		setting, err = runner.api.UniversalSSLSettingDetails(runner.Context, zone.ID)
		if err != nil {
			return
		}
		audit.UniversalSSL = setting.Enabled
		log.Printf("loading Universal SSL setting: %t", audit.UniversalSSL)
	}

	{
		log.Printf("loading records...")
		var records []cloudflare.DNSRecord
		records, err = runner.api.DNSRecords(runner.Context, zone.ID, cloudflare.DNSRecord{})
		if err != nil {
			return
		}
		addRecords(&audit, records)
		log.Printf("loading records %d", len(audit.Records))
	}

	// TODO: in the future maybe: https://api.cloudflare.com/#secondary-dns-properties

	{
		log.Printf("loading spectrum apps...")
		var spectrumApps []cloudflare.SpectrumApplication
		spectrumApps, err = runner.api.SpectrumApplications(runner.Context, zone.ID)
		if err != nil {
			return
		}
		addSpectrumApps(&audit, spectrumApps)
		log.Printf("loading spectrum apps %d", len(audit.SpectrumApps))
	}

	{
		log.Printf("loading TLS certificate packs...")
		var certificatePacks []types.CFCertificatePack
		certificatePacks, err = runner.listCertificatePacks(zone.ID)
		if err != nil {
			return
		}
		addSSL(&audit, certificatePacks)
		log.Printf("loading TLS certificate packs %d", len(audit.Certs))
	}

	{
		log.Printf("loading Custom TLS certificates...")
		var certs []types.CFCert
		certs, err = runner.listCustomCertificates(zone.ID)
		if err != nil {
			return
		}
		for _, cert := range certs {
			addCert(&audit, cert, nil)
		}
		log.Printf("loading Custom TLS certificates %d", len(audit.Certs))
	}

	{
		log.Printf("loading page rules...")
		var pageRules []cloudflare.PageRule
		pageRules, err = runner.api.ListPageRules(runner.Context, zone.ID)
		if err != nil {
			return
		}
		addPageRules(&audit, pageRules)
		log.Printf("loading page rules %d", len(audit.PageRules))
	}

	// These are the rules we don't want to use!
	// These also include account and global access rules, that apply to the zone.
	{
		log.Printf("loading deprecated firewall access rules...")
		var accessRules []cloudflare.AccessRule
		accessRules, err = runner.listZoneAccessRules(zone.ID)
		if err != nil {
			return
		}
		addDeprecatedFWAccessRules(&audit, accessRules)
		log.Printf("loading deprecated firewall access rules %d", len(audit.DeprecatedFWAccessRules))
	}

	{
		err = runner.auditWAFByZone(log, &audit, zone.ID)
		if err != nil {
			return
		}
	}

	{
		log.Printf("loading WAF overrides...")
		var wafOverrides []cloudflare.WAFOverride
		wafOverrides, err = runner.api.ListWAFOverrides(runner.Context, zone.ID)
		if err != nil {
			code := 0
			if apiErr, ok := err.(*cloudflare.APIRequestError); ok {
				code = apiErr.StatusCode
			}
			// Ignore this while they don't offer Token support for now.
			if code == 405 || code == 403 || strings.Contains(err.Error(), "API Tokens are not supported") {
				log.Warnf("loading WAF overrides: unsupported (issue #1)")
				// https://gitlab.com/gitlab-com/gl-infra/cloudflare-audit/-/issues/1
				err = nil
			} else {
				return
			}
		}
		addWAFOverrides(&audit, wafOverrides)
		log.Printf("loading WAF overrides %d", len(audit.WAFOverrides))
	}

	{
		log.Printf("loading User-Agent rules...")
		var page = 1
		var maxPage = 1
		for page <= maxPage {
			var tmp *cloudflare.UserAgentRuleListResponse
			tmp, err = runner.api.ListUserAgentRules(runner.Context, zone.ID, page)
			if err != nil {
				return
			}
			for _, rule := range tmp.Result {
				if isRuleTemporary(rule.Description) {
					audit.TemporaryUserAgentRules[rule.ID] = rule
				} else {
					audit.UserAgentRules[rule.ID] = rule
				}
			}
			maxPage = tmp.TotalPages
			page++
		}
		log.Printf("loading User-Agent rules %d, %d temporary", len(audit.UserAgentRules), len(audit.TemporaryUserAgentRules))
	}

	{
		log.Printf("loading Zone settings...")
		var tmp *cloudflare.ZoneSettingResponse
		tmp, err = runner.api.ZoneSettings(runner.Context, zone.ID)
		if err != nil {
			return
		}
		for _, setting := range tmp.Result {
			audit.Settings[setting.ID] = setting
		}
		log.Printf("loading Zone settings %d", len(audit.Settings))
	}

	{
		log.Printf("loading Logpush jobs...")
		var tmp []cloudflare.LogpushJob
		tmp, err = runner.api.LogpushJobs(runner.Context, zone.ID)
		if err != nil {
			if strings.Contains(err.Error(), "invalid character 'U'") {
				log.Warnf("loading Logpush jobs: unsupported (issue #2)")
				log.Warnf("loading Logpush jobs: Workaround: ensure the login has `logs:edit` permissions")
				// https://gitlab.com/gitlab-com/gl-infra/cloudflare-audit/-/issues/2
				err = nil
			} else {
				return
			}
		}
		for _, job := range tmp {
			id := fmt.Sprintf("%d", job.ID)
			// Strip variable info
			job.ErrorMessage = ""
			job.LastComplete = nil
			job.LastError = nil
			job.OwnershipChallenge = ""
			audit.LogpushJobs[id] = job
		}
		log.Printf("loading Logpush jobs %d", len(audit.LogpushJobs))
	}

	{
		log.Printf("loading Custom pages...")
		var tmp []cloudflare.CustomPage
		tmp, err = runner.api.CustomPages(runner.Context, &cloudflare.CustomPageOptions{
			ZoneID: audit.ID,
		})
		if err != nil {
			return
		}
		for _, page := range tmp {
			if page.State != "default" {
				audit.CustomPages[page.ID] = page
			}
		}
		log.Printf("loading Custom pages %d", len(audit.CustomPages))
	}

	{
		log.Printf("loading Worker routes...")
		var tmp cloudflare.WorkerRoutesResponse
		tmp, err = runner.api.ListWorkerRoutes(runner.Context, audit.ID)
		if err != nil {
			return
		}
		for _, route := range tmp.Routes {
			audit.WorkerRoutes[route.ID] = route
		}
		log.Printf("loading Worker pages %d", len(audit.WorkerRoutes))
	}

	{
		log.Printf("loading Rate limits...")
		var tmp []cloudflare.RateLimit
		tmp, err = runner.api.ListAllRateLimits(runner.Context, audit.ID)
		if err != nil {
			return
		}
		for _, limit := range tmp {
			if isRuleTemporary(limit.Description) {
				audit.TemporaryRateLimits[limit.ID] = limit
			} else {
				audit.RateLimits[limit.ID] = limit
			}
		}
		log.Printf("loading Rate limits %d, %d temporary", len(audit.RateLimits), len(audit.TemporaryRateLimits))
	}

	{
		log.Printf("loading Lockdown rules...")
		var page = 1
		var maxPage = 1
		for page <= maxPage {
			var tmp *cloudflare.ZoneLockdownListResponse
			tmp, err = runner.api.ListZoneLockdowns(runner.Context, zone.ID, page)
			if err != nil {
				return
			}
			for _, rule := range tmp.Result {
				audit.LockdownRules[rule.ID] = rule
			}
			maxPage = tmp.TotalPages
			page++
		}
		log.Printf("loading Lockdown rules %d", len(audit.LockdownRules))
	}

	{
		log.Printf("loading Firewall rules...")
		var tmp []cloudflare.FirewallRule
		tmp, err = runner.getAllFirewallRules(zone.ID)
		if err != nil {
			return
		}
		for _, rule := range tmp {
			if isRuleTemporary(rule.Description) {
				audit.TemporaryFirewallRules[rule.ID] = rule
			} else {
				audit.FirewallRules[rule.ID] = rule
			}
		}
		log.Printf("loading Firewall rules %d, %d temporary", len(audit.FirewallRules), len(audit.TemporaryFirewallRules))
	}

	return
}

func isRuleTemporary(description string) bool {
	splits := strings.SplitN(description, "|", 3)

	// Description is an optional field. so we only really care about 3 parts
	if len(splits) < 3 {
		return false
	} else {
		// https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/cloudflare/managing-traffic.md#description-format-of-cloudflare-rules
		return strings.HasPrefix(strings.TrimSpace(splits[2]), "temporary")
	}
}

func addWAFOverrides(audit *types.ZoneAudit, overrides []cloudflare.WAFOverride) {
	for _, override := range overrides {
		audit.WAFOverrides[override.ID] = override
	}
}

func addDeprecatedFWAccessRules(audit *types.ZoneAudit, rules []cloudflare.AccessRule) {
	for _, source := range rules {
		audit.DeprecatedFWAccessRules[source.ID] = types.DeprecatedFWAccessRuleAudit{
			ID:          source.ID,
			Nodes:       source.Notes,
			Mode:        source.Mode,
			TargetType:  source.Configuration.Target,
			TargetValue: source.Configuration.Value,
			ModTime:     source.ModifiedOn.Unix(),
			Scope:       source.Scope.Type,
		}
	}
}

func (runner *AuditRunner) listZoneAccessRules(zoneID string) (accessRules []cloudflare.AccessRule, err error) {
	var pages = 1
	var page = 1

	for page <= pages {
		var tmp *cloudflare.AccessRuleListResponse
		tmp, err = runner.api.ListZoneAccessRules(runner.Context, zoneID, cloudflare.AccessRule{}, page)
		if err != nil {
			return
		}
		pages = tmp.TotalPages
		page++
		accessRules = append(accessRules, tmp.Result...)
	}
	return
}

func addPageRules(audit *types.ZoneAudit, rules []cloudflare.PageRule) {
	for _, source := range rules {
		var rule types.PageRuleAudit = types.PageRuleAudit{
			ID:       source.ID,
			Actions:  map[string]interface{}{},
			Priority: source.Priority,
			Status:   source.Status,
			ModTime:  source.ModifiedOn.Unix(),
		}

		for _, target := range source.Targets {
			rule.Targets = append(rule.Targets, target.Constraint.Value)
		}

		for _, action := range source.Actions {
			rule.Actions[cloudflare.PageRuleActions[action.ID]] = action.Value
		}

		audit.PageRules[rule.ID] = rule
	}
}

func addRecords(audit *types.ZoneAudit, records []cloudflare.DNSRecord) {
	for _, record := range records {
		audit.Records[record.ID] = types.RecordAudit{
			ID:      record.ID,
			Name:    record.Name,
			Type:    record.Type,
			Content: record.Content,
			ModTime: record.ModifiedOn.Unix(),
		}
	}
}

func addSpectrumApps(audit *types.ZoneAudit, spectrumApps []cloudflare.SpectrumApplication) {
	for _, source := range spectrumApps {
		var app types.SpectrumAudit = types.SpectrumAudit{
			ID:               source.ID,
			Protocol:         source.Protocol,
			Name:             source.DNS.Name,
			IPFirewall:       source.IPFirewall,
			ProxyProtocol:    string(source.ProxyProtocol),
			TLS:              source.TLS,
			TrafficType:      source.TrafficType,
			ArgoSmartRouting: source.ArgoSmartRouting,
			ModTime:          source.ModifiedOn.Unix(),
		}

		if len(source.OriginDirect) > 0 {
			app.Origin = append(app.Origin, source.OriginDirect...)
		}
		if source.OriginDNS != nil {
			app.Origin = append(app.Origin, fmt.Sprintf("DNS=%s:%d", source.OriginDNS.Name, source.OriginPort))
		}

		// If we have allocated IPs, assign them here. `dynamic` otherwise
		if source.EdgeIPs != nil {
			if source.EdgeIPs.Connectivity != nil {
				if source.EdgeIPs.Connectivity.Dynamic() {
					app.EdgeIPs = append(app.EdgeIPs, string(cloudflare.SpectrumEdgeTypeDynamic))
				} else {
					for _, ip := range source.EdgeIPs.IPs {
						app.EdgeIPs = append(app.EdgeIPs, ip.String())
					}
				}
			} else {
				app.EdgeIPs = append(app.EdgeIPs, string(source.EdgeIPs.Type))
			}
		} else {
			app.EdgeIPs = append(app.EdgeIPs, string(cloudflare.SpectrumEdgeTypeDynamic))
		}

		audit.SpectrumApps[app.ID] = app
	}
}

func addCert(audit *types.ZoneAudit, sourceCert types.CFCert, primaryCertificate json.Token) {
	var id string
	switch sourceCert.ID.(type) {
	case string:
		id = sourceCert.ID.(string)
	case float64:
		// Sometimes this is a number, not a string...
		id = fmt.Sprintf("%.0f", sourceCert.ID.(float64))
	default:
		panic(fmt.Errorf("type of ID is %s. Unsupported. Content: %+v", reflect.TypeOf(sourceCert.ID), sourceCert.ID))
	}

	if _, exists := audit.Certs[id]; exists {
		return
	}

	var cert = types.CertAudit{
		ID:              id,
		Hosts:           sourceCert.Hosts,
		Issuer:          sourceCert.Issuer,
		Signature:       sourceCert.Signature,
		Status:          sourceCert.Status,
		GeoRestrictions: sourceCert.GeoRestrictions.Label,
		ModTime:         sourceCert.ModifiedOn.Unix(),
		Expiry:          sourceCert.ExpiresOn.Unix(),
		Priority:        sourceCert.Priority,
		Primary:         sourceCert.ID == primaryCertificate && primaryCertificate != nil,
		Active:          sourceCert.Status == "active",
	}

	audit.Certs[cert.ID] = cert
}

func addSSL(audit *types.ZoneAudit, ssl []types.CFCertificatePack) {
	for _, source := range ssl {
		for _, sourceCert := range source.Certificates {
			addCert(audit, sourceCert, source.PrimaryCertificate)
		}
	}
}
