package main

import (
	"gitlab.com/gitlab-com/gl-infra/cloudflare-audit/AuditRunner"
	"gitlab.com/gitlab-com/gl-infra/cloudflare-audit/ReportGenerator"
	"gitlab.com/gitlab-com/gl-infra/cloudflare-audit/types"
	"os"
	"path"
)

func main() {
	var runner *AuditRunner.AuditRunner
	var err error
	{
		token := os.Getenv("CF_API_TOKEN")
		if token == "" {
			panic("no CF_API_TOKEN")
		}
		runner, err = AuditRunner.NewAuditRunner(token)
		if err != nil {
			panic(err)
		}
	}

	{
		var accounts []types.AccountAudit
		for _, accID := range runner.Accounts {
			var audit types.AccountAudit
			// TODO: Make this an interface and unify
			audit, err = runner.AuditAccount(accID)
			if err != nil {
				panic(err)
			}
			accounts = append(accounts, audit)
		}

		err = ReportGenerator.GenerateAccountReports(accounts, path.Join("reports", "accounts"))
		if err != nil {
			panic(err)
		}
	}

	{
		var zones []types.ZoneAudit
		for _, zone := range runner.Zones {
			var audit types.ZoneAudit
			// TODO: Make this an interface and unify
			audit, err = runner.AuditZone(zone)
			if err != nil {
				break
			}
			zones = append(zones, audit)
		}
		if err != nil {
			panic(err)
		}
		err = ReportGenerator.GenerateZoneReports(zones, path.Join("reports", "zones"))
		if err != nil {
			panic(err)
		}
	}

	// TODO: User level audit:
}
