package types

import (
	"encoding/json"
	"fmt"
	"github.com/cloudflare/cloudflare-go"
	"time"
)

type CFCert struct {
	ID              json.Token                              `json:"id,omitempty"`
	Hosts           []string                                `json:"hosts"`
	Issuer          string                                  `json:"issuer"`
	Signature       string                                  `json:"signature"`
	Status          string                                  `json:"status"`
	BundleMethod    string                                  `json:"bundle_method"`
	GeoRestrictions cloudflare.ZoneCustomSSLGeoRestrictions `json:"geo_restrictions,omitempty"`
	ZoneID          string                                  `json:"zone_id"`
	UploadedOn      time.Time                               `json:"uploaded_on"`
	ModifiedOn      time.Time                               `json:"modified_on"`
	ExpiresOn       time.Time                               `json:"expires_on"`
	Priority        int                                     `json:"priority"`
	KeylessServer   *cloudflare.KeylessSSL                  `json:"keyless_server,omitempty"`
}

//Cloudflare does not expose this in cloudflare-go
type CFCertificatePack struct {
	ID                 string     `json:"id"`
	Type               string     `json:"type"`
	Hosts              []string   `json:"hosts"`
	Certificates       []CFCert   `json:"certificates"`
	PrimaryCertificate json.Token `json:"primary_certificate,omitempty"`
}

// https://api.cloudflare.com/#dnssec-properties
type CFDNSSEC struct {
	Digest          string    `json:"digest,omitempty"`
	Flags           string    `json:"flags,omitempty"`
	PublicKey       string    `json:"public_key,omitempty"`
	Algorithm       string    `json:"algorithm,omitempty"`
	DS              string    `json:"ds,omitempty"`
	Status          string    `json:"status"`
	DigestAlgorithm string    `json:"digest_algorithm,omitempty"`
	DigestType      string    `json:"digest_type,omitempty"`
	ModifiedOn      time.Time `json:"modified_on,omitempty"`
	KeyType         string    `json:"key_type,omitempty"`
	KeyTag          int       `json:"key_tag,omitempty"`
}

type RecordAudit struct {
	ID      string `json:"id"`
	Name    string `json:"name"`
	Type    string `json:"type"`
	Content string `json:"content"`
	ModTime int64  `json:"mod_time"`
}

type SpectrumAudit struct {
	ID               string   `json:"id"`
	Protocol         string   `json:"protocol"`
	Name             string   `json:"name,omitempty"`
	Origin           []string `json:"origin"`
	IPFirewall       bool     `json:"ip_firewall"`
	ProxyProtocol    string   `json:"proxy_protocol"`
	TLS              string   `json:"tls"`
	TrafficType      string   `json:"traffic_type"`
	EdgeIPs          []string `json:"edge_ips"`
	ArgoSmartRouting bool     `json:"argo_smart_routing"`
	ModTime          int64    `json:"mod_time"`
}

type CertAudit struct {
	ID              string   `json:"id"`
	Hosts           []string `json:"hosts"`
	Issuer          string   `json:"issuer"`
	Signature       string   `json:"signature"`
	Status          string   `json:"status"`
	GeoRestrictions string   `json:"geo_restrictions,omitempty"`
	ModTime         int64    `json:"mod_time"`
	Expiry          int64    `json:"expiry"`
	Priority        int      `json:"priority"`
	Primary         bool     `json:"primary"`
	Active          bool     `json:"active"`
}

type PageRuleAudit struct {
	ID       string                 `json:"id"`
	Targets  []string               `json:"targets"`
	Actions  map[string]interface{} `json:"actions"`
	Priority int                    `json:"priority"`
	Status   string                 `json:"status"`
	ModTime  int64                  `json:"mod_time"`
}

type DeprecatedFWAccessRuleAudit struct {
	ID          string `json:"id"`
	Nodes       string `json:"nodes"`
	Mode        string `json:"mode"`
	TargetType  string `json:"target_type"`
	TargetValue string `json:"target_value"`
	ModTime     int64  `json:"mod_time"`
	Scope       string `json:"scope"`
}

type WAFAuditRule struct {
	ID          string `json:"id"`
	Description string `json:"description"`
	Priority    string `json:"priority"` // This is a string in the CF docs, too.
	Mode        string `json:"mode"`
	DefaultMode string `json:"default_mode"`
}

type WAFAuditGroup struct {
	ID            string                  `json:"id"`
	Name          string                  `json:"name"`
	Description   string                  `json:"description"`
	ModifiedRules int                     `json:"modified_rules"`
	Mode          string                  `json:"mode"`
	Rules         map[string]WAFAuditRule `json:"rules"`
}

func (grp *WAFAuditGroup) CountWAFRules() int {
	return len(grp.Rules)
}

type WAFAuditPackage struct {
	ID            string                   `json:"id"`
	Name          string                   `json:"name"`
	Description   string                   `json:"description"`
	DetectionMode string                   `json:"detection_mode"`
	Sensitivity   string                   `json:"sensitivity"`
	ActionMode    string                   `json:"action_mode"`
	Groups        map[string]WAFAuditGroup `json:"-"`
}

func (pkg *WAFAuditPackage) CountWAFRules() (out int) {
	for _, grp := range pkg.Groups {
		out += grp.CountWAFRules()
	}
	return
}

func (pkg *WAFAuditPackage) CountWAFGroups() int {
	return len(pkg.Groups)
}

type ZoneAudit struct {
	ID                      string                                 `json:"id"`
	Name                    string                                 `json:"name"`
	DevMode                 int                                    `json:"development_mode"`
	ModTime                 int64                                  `json:"mod_time"`
	NameServers             []string                               `json:"name_servers"`
	Status                  string                                 `json:"status"`
	Paused                  bool                                   `json:"paused"`
	Type                    string                                 `json:"type"`
	VanityNS                []string                               `json:"vanity_name_servers"`
	DeactReason             string                                 `json:"deactivation_reason"`
	Records                 map[string]RecordAudit                 `json:"-"` // Not exported automatically
	SpectrumApps            map[string]SpectrumAudit               `json:"-"` // Not exported automatically
	Certs                   map[string]CertAudit                   `json:"-"` // Not exported automatically
	PageRules               map[string]PageRuleAudit               `json:"-"` // Not exported automatically
	DeprecatedFWAccessRules map[string]DeprecatedFWAccessRuleAudit `json:"-"` // Not exported automatically
	WAF                     map[string]WAFAuditPackage             `json:"-"` // Not exported automatically
	WAFOverrides            map[string]cloudflare.WAFOverride      `json:"-"` // Not exported automatically
	UserAgentRules          map[string]cloudflare.UserAgentRule    `json:"-"` // Not exported automatically
	TemporaryUserAgentRules map[string]cloudflare.UserAgentRule    `json:"-"` // Not exported automatically
	Settings                map[string]cloudflare.ZoneSetting      `json:"-"` // Not exported automatically
	LogpushJobs             map[string]cloudflare.LogpushJob       `json:"-"` // Not exported automatically
	CustomPages             map[string]cloudflare.CustomPage       `json:"-"` // Not exported automatically
	WorkerRoutes            map[string]cloudflare.WorkerRoute      `json:"-"` // Not exported automatically
	RateLimits              map[string]cloudflare.RateLimit        `json:"-"` // Not exported automatically
	TemporaryRateLimits     map[string]cloudflare.RateLimit        `json:"-"` // Not exported automatically
	LockdownRules           map[string]cloudflare.ZoneLockdown     `json:"-"` // Not exported automatically
	FirewallRules           map[string]cloudflare.FirewallRule     `json:"-"` // Not exported automatically
	TemporaryFirewallRules  map[string]cloudflare.FirewallRule     `json:"-"` // Not exported automatically
	DNSSEC                  *CFDNSSEC                              `json:"dnssec"`
	UniversalSSL            bool                                   `json:"universal_ssl_enabled"`
	ArgoSmartRouting        bool                                   `json:"argo_smart_routing"`
}

func (audit *ZoneAudit) Initialize() {
	audit.ID = ""
	audit.Name = ""
	audit.Records = map[string]RecordAudit{}
	audit.SpectrumApps = map[string]SpectrumAudit{}
	audit.Certs = map[string]CertAudit{}
	audit.PageRules = map[string]PageRuleAudit{}
	audit.DeprecatedFWAccessRules = map[string]DeprecatedFWAccessRuleAudit{}
	audit.WAF = map[string]WAFAuditPackage{}
	audit.WAFOverrides = map[string]cloudflare.WAFOverride{}
	audit.UserAgentRules = map[string]cloudflare.UserAgentRule{}
	audit.Settings = map[string]cloudflare.ZoneSetting{}
	audit.LogpushJobs = map[string]cloudflare.LogpushJob{}
	audit.CustomPages = map[string]cloudflare.CustomPage{}
	audit.WorkerRoutes = map[string]cloudflare.WorkerRoute{}
	audit.RateLimits = map[string]cloudflare.RateLimit{}
	audit.LockdownRules = map[string]cloudflare.ZoneLockdown{}
	audit.FirewallRules = map[string]cloudflare.FirewallRule{}
	audit.TemporaryFirewallRules = map[string]cloudflare.FirewallRule{}
	audit.TemporaryUserAgentRules = map[string]cloudflare.UserAgentRule{}
	audit.TemporaryRateLimits = map[string]cloudflare.RateLimit{}
	audit.DNSSEC = nil
	audit.UniversalSSL = false
	audit.ArgoSmartRouting = false
}

func (audit *ZoneAudit) CountWAFRules() (out int) {
	for _, pkg := range audit.WAF {
		out += pkg.CountWAFRules()
	}
	return
}

func (audit *ZoneAudit) CountWAFGroups() (out int) {
	for _, pkg := range audit.WAF {
		out += pkg.CountWAFGroups()
	}
	return
}

func (audit *ZoneAudit) Summarize() (out string) {
	out += fmt.Sprintf("Zone: %s (ID: %s)\n", audit.Name, audit.ID)
	out += fmt.Sprintf("Zone settings: %d\n", len(audit.Settings))
	if audit.DNSSEC != nil {
		out += fmt.Sprintf("DNSSEC: %s\n", audit.DNSSEC.Status)
	} else {
		out += fmt.Sprintf("DNSSEC: unconfigured\n")
	}
	out += fmt.Sprintf("Universal SSL: %t\n", audit.UniversalSSL)
	out += fmt.Sprintf("Argo Smart Routing: %t\n", audit.ArgoSmartRouting)
	out += fmt.Sprintf("Records: %d\n", len(audit.Records))
	out += fmt.Sprintf("Spectrum Apps: %d\n", len(audit.SpectrumApps))
	out += fmt.Sprintf("Certs: %d\n", len(audit.Certs))
	out += fmt.Sprintf("PageRules: %d\n", len(audit.PageRules))
	out += fmt.Sprintf("Deprecated FW AccessRules: %d (we don't want these!)\n", len(audit.DeprecatedFWAccessRules))
	out += fmt.Sprintf("WAF rule sets: %d\n", len(audit.WAF))
	out += fmt.Sprintf("WAF groups: %d\n", audit.CountWAFGroups())
	out += fmt.Sprintf("WAF rules: %d\n", audit.CountWAFRules())
	out += fmt.Sprintf("WAF overrides: %d\n", len(audit.WAFOverrides))
	out += fmt.Sprintf("User-Agent rules: %d\n", len(audit.UserAgentRules))
	out += fmt.Sprintf("temp. User-Agent rules: %d\n", len(audit.TemporaryUserAgentRules))
	out += fmt.Sprintf("Logpush jobs: %d\n", len(audit.LogpushJobs))
	out += fmt.Sprintf("Custom pages: %d\n", len(audit.CustomPages))
	out += fmt.Sprintf("Worker routes: %d\n", len(audit.WorkerRoutes))
	out += fmt.Sprintf("Rate limits: %d\n", len(audit.RateLimits))
	out += fmt.Sprintf("temp. Rate limits: %d\n", len(audit.TemporaryRateLimits))
	out += fmt.Sprintf("Lockdown rules: %d\n", len(audit.LockdownRules))
	out += fmt.Sprintf("Firewall rules: %d\n", len(audit.FirewallRules))
	out += fmt.Sprintf("temp. Firewall rules: %d\n", len(audit.TemporaryFirewallRules))

	return
}

type MemberAudit struct {
	ID     string                              `json:"id"`
	Code   string                              `json:"code"`
	User   cloudflare.AccountMemberUserDetails `json:"user"`
	Status string                              `json:"status"`
	Roles  []string                            `json:"roles"`
}

type AccountAudit struct {
	ID          string                            `json:"id,omitempty"`
	Name        string                            `json:"name,omitempty"`
	Settings    *cloudflare.AccountSettings       `json:"settings"`
	CustomPages map[string]cloudflare.CustomPage  `json:"-"` // Not exported automatically
	Members     map[string]MemberAudit            `json:"-"` // Not exported automatically
	Roles       map[string]cloudflare.AccountRole `json:"-"` // Not exported automatically
}
