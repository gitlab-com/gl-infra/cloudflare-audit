module gitlab.com/gitlab-com/gl-infra/cloudflare-audit

go 1.16

require (
	github.com/cloudflare/cloudflare-go v0.26.0
	github.com/sirupsen/logrus v1.8.1
)

replace golang.org/x/text => golang.org/x/text v0.3.3 // Fixes CVE-2020-14040

replace gopkg.in/yaml.v2 => gopkg.in/yaml.v2 v2.2.3 // Fixes GMS-2019-2
