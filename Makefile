# Required for -buildmode=pie
CGO_ENABLED		=	1
# Compile with some tweaks for security and reproducibility
GO_COMMON_FLAGS	=	-trimpath -ldflags="-w -s -extldflags=-Wl,-z,now,-z,relro" -buildmode=pie
GO_DEPS			=	$(shell find . -wholename '**/*.go' -o -iname 'go.sum' -o -iname 'go.mod')
export CGO_ENABLED

.PHONY: all clean run

all: cloudflare-audit

clean:
	$(RM) ./cloudflare-audit ./cloudflare-test ./tests.xml
	$(RM) -r reports/

run: cloudflare-audit .env
	. ./.env && ./$<

%: cmd/%.go $(GO_DEPS) Makefile
	go build -v $(GO_COMMON_FLAGS) -o $@ $<
