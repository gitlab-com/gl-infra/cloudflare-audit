FROM golang:1.16-alpine AS compile
ENV CGO_ENABLED=0
COPY . /app
RUN apk add --no-cache build-base ca-certificates
RUN make -C /app cloudflare-audit

FROM alpine:latest
RUN apk --no-cache add ca-certificates
COPY --from=compile /app/cloudflare-audit /bin/cloudflare-audit
ENTRYPOINT ["/bin/cloudflare-audit"]
